# CAR CLIMA #

This is an application created in the workshop *"Hella Embedded Club"*, through which we can control air conditioning installation of a car.
Functionality: temperature control is achieved through a potentiometer.Depending on the desired temperature microcontroller starts fans / heating system.

![img](http://s33.postimg.org/a5oibbepr/Untitled.png)
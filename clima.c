

/* 
 * File:   clima.c
 * Author: Marius Iliescu
 *
 * Created on February 2, 2014, 11:39 PM
 */

#include <stdio.h>
#include <stdlib.h>

#include <p18f8722.h>

#include "clima.h"
#include "lcd.h"
#include "uart.h"

// configuration bits
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))









#define DBG_MSG 0
#if (DBG_MSG == 1)
#define DBG(x)  UART_puts((char *)x)    // debug messages on UART
#else
#define DBG(x)                  // debug messages are lost
#endif


typedef unsigned char byte;
#define ON          1
#define OFF         0
#define TEMP_STEP   1
#define FAN_STEPS   6
#define TEMP_MIN    18


unsigned int ADCRead(unsigned char ch);
void checkInputs(void);
byte getOnOffButton(void);
void setLcd(void);
void updateLcd(void);
void checkInputs(void);
void stateMachine(void);
void initButtons(void);
void initAdc(void);
void initPwm(void);
void init(void);
void main(void);



byte leftButtonEv = 0;
byte rightButtonEv = 0;
byte setTemp = 0;
byte inTemp = 0;
unsigned int outTemp = 0;

unsigned char tick = 0;
unsigned char ev = 0;
unsigned char cnt = 0;

/* used to format print messages */
char msg[20] = {0};



void setLcd(void)
{

} /* void setLcd(state_e state) */



/*******************************************************************************
 * Check Inputs Function
 */
void updateLcd(void)
{

} /* void updateLcd(state_e state) */



/*******************************************************************************
 * Check Inputs Function
 */
void checkInputs(void)
{
/* RB0 - check left push button event */
    /* TODO: Read DI */

    /* TODO: generate the leftButtonEv if button was pressed (1=>0)*/

/* RA5 - check right push button event */
    /* TODO: Read DI */

    /* TODO: generate the rightButtonEv if button was pressed (1=>0)*/

/* read ADC AN0 - on board potentiometer */
    /* TODO: read from ADC the value for potentiometer */

    /* TODO: (optional) convert in temperature setting steps */

/* read ADC AN1 - on board temperature sensor */
    /* MCP9701: 19mV/*C */
    /* TODO: read analog value */
    
    /* TODO: (optional) convert raw value to temp */

/* read ADC AN3 - inside temperature sensor */
    /* LM35: 10mV/*C
    /* TODO: read analog value */

    /* TODO: (optional) convert raw value to temp */
        
} /* void checkInputs(void) */



/*******************************************************************************
 * State Machine Function
 */
void stateMachine(void)
{
    /* TODO: to be done on last meeting */

} /* void stateMachine(void) */



/*******************************************************************************
 * Update outputs Function
 */
void updateOutputs(void)
{
    /* TODO: to be done on "Outputs" meeting */
} /* void updateOutputs(void) */



/*******************************************************************************
 * Init Buttons Function
 */
void initButtons(void)
{
    /* RB0 left push button */
    TRISB0 = 1; /* only RB0 as input */
    
    /* RA5 right push button */
    TRISA5 = 1; /* only RA5 as input */

} /* void initButtons(void) */



/*******************************************************************************
 * Init Buttons Function
 */
void initAdc(void)
{
    /* RA0 potentiometer */
    TRISA = TRISA | (1<<0); /* RA0 as input */

    /* RA1 onboard temperature sensor */
    TRISA = TRISA | (1<<1); /* RA1 as input */

    /* RA4 outside temperature sensor */
    TRISA = TRISA | (1<<4); /* RA4 as input */

    TRISA = 0xFF;

// ADCON0
    ADCON0bits.CHS = 1;         // channel AN0
    ADCON0bits.GO_nDONE = 0;    // ADC Idle
    ADCON0bits.ADON = 0;        // turn OFF ADC module
// ADCON1
    ADCON1bits.VCFG = 0b00;     // Voltage ref AVdd AVss
    ADCON1bits.PCFG = 0b0000;   // A/D Port config AN0-AN4 analog, AN5-AN15 digital
// ADCON2
    ADCON2bits.ADFM = 1;        // 0=left, 1=right justified
    ADCON2bits.ACQT = 0b111;    // A/D Acquisition time 20 Tad
    ADCON2bits.ADCS = 0b010;    // A/D Conversion clock Fosc/32
} /* void initButtons(void) */



/*******************************************************************************
 * Init Buttons Function
 */
unsigned int ADCRead(unsigned char ch)
{
   if(ch>13) return 0;  //Invalid Channel

   ADCON0bits.ADON = 1;     // disable AD module
   ADCON0bits.CHS = ch;      // select channel
   ADCON0bits.ADON = 1;     // switch on the adc module
   ADCON0bits.GO_nDONE = 1; //Start conversion
   while(ADCON0bits.GO_nDONE); //wait for the conversion to finish
   //ADCON0bits.ADON = 0;  //switch off adc

   return ADRES;
} /* unsigned int ADCRead(unsigned char ch) */



/*******************************************************************************
 * Init PWM Function
 */
void initPwm(void)
{

} /* void initPwm(void) */



void initTmr(void)
{
    PORTJbits.RJ6 = 0;
    PORTJbits.RJ7 = 0;
    TRISJbits.TRISJ7 = 0;   // This sets pin RB7 to output
    TRISJbits.TRISJ6 = 0;   // This sets pin RB6 to output

// START - TMR0 setup
    TMR0 = 0;
    T0CON = 0;
    T0CONbits.TMR0ON = 0;   // timer OFF
    T0CONbits.T08BIT = 0;   // 16bit timer
    T0CONbits.T0CS = 0;     // internal source clock
    T0CONbits.T0SE = 0;     // source edge Low-2-High
    T0CONbits.PSA = 0;      // prescaler active
    T0CONbits.T0PS = 0;     // prescaler 3 bits (Fosc/4)/presc 1:2 => timer clock = (10MHz/4) / 2 = 1.25Mhz
    /* 1000Hz */
    /* 1khz => 1ms period
     * 1.25Mhz / 1khz => 1250
     * 65535 - 1250 = 64285 = FB1Dx;
     */
    TMR1H = 0xFB;           //
    TMR1L = 0x1D;           //
    T0IE = 1; //enable TMR0 overflow interrupts
    GIE = 1; //enable Global interrupts
    T0CONbits.TMR0ON = 1;   // timer ON
// END - TMR0 setup
} /* void initTmr(void) */



// Interrupt Service Routine (keyword "interrupt" tells the compiler it's an ISR)
void interrupt ISR(void)
{
    // TMR0 interrupt

    // process other interrupt sources here, if required
}



/*******************************************************************************
 * Init Function
 */
void init(void)
{
    TRISD=0;
    PORTD=0;
    MEMCONbits.EBDIS=1;

    /* init UART */
    UART_Init();
    UART_puts((char *)"\n\rInitializing...\n\r");

    /* init buttons */
    initButtons();

    /* init ADC */
    initAdc();

    /* init PWM */
    initPwm();

    /* init TMR */
    //initTmr();

    /* init LCD */
    LcdInit();

/* START - transition from "Power OFF" to "OFF"*/
/* TODO: fill in */
/* END - transition from "Power OFF" to "OFF"*/
} /* void init(void) */



/*******************************************************************************
 * Main Function
 */
void main(void)
{
    init();

    while(1)
    {
        //while (ev == 0);
        ev = 0;

        checkInputs();
        stateMachine();
        updateOutputs();

        /* clear events */
        leftButtonEv = 0; /* clear event from left button */
        rightButtonEv = 0; /* clear event from right button */
    }
} /* void main(void) */




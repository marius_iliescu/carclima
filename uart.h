#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif


char UART_Init(void);
void UART_puts(char *s);

#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

